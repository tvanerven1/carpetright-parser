import json
import itertools

""""
Level: polygon

Mapping: productCategory

10001 = Gordijn


Mapping: productionMethod

1100 = Gordijn Vliesband (plooigordijn) 

"""

# def analyse_property(id, description, key, value, visible):
#     if not visible:
#         return
#     if id:
#         print(f'id: {id}, description: {description} , key: {key}, value: {value}')

#     # If aantal > meerdere orders aanmaken, alleen bij hoofdstof:

#     # If Equal -> orders

#     # If overhevelen...


class CarpetRightJSONParser():
    """
    Nifty little class to parse the CarpetRight JSON files. Reads file,
    parses, then dumps curtain in curtains for iteration. Steps:

    1. Identify materials for curtain and extract ids ()
    """

    CURTAIN_TYPES = {
        '1800': 'pleatcurtain',
    }

    PLEATCURTAIN_TRANSLATOR = {
        'Curtain.Fabric.RoomHigh': 'width_type',
        'Curtain.Fabric.PleatDistance': 'pleat_distance',
        'Curtain.Fabric.PleatFactor': 'pleat_factor',
        'Curtain.Fabric.ReturnPleat': 'return_pleat',
        'Curtain.Fabric.UnderHem': 'hem_type',
        'Curtain.Fabric.WeightCord': 'with_weightlace',
    }

    PLEATCURTAIN_DESCRIPTION_TRANSLATOR = {
        'Gordijnbreedte': 'width',
        'sType': 'pleat_type',
        'Hoofdje vliesband': 'head_size',
    }

    RINGCURTAIN_TRANSLATOR = {}

    FRONSLINTCURTAIN_TRANSLATOR = {}

    def __init__(self, filename):
        """Private members are data containers, public members results."""
        self.order_id = None
        self.generated_curtains = []
        self.filename = filename
        self.data = self._open_file()
        self._project_materials = {}
        self._set_fabrics()
        self._enrich_project_materials()
        self._match_lining_to_curtain()
        self._set_materials()
        self._identify_curtain_types()
        self._analyse_curtain_properties()
        from IPython import embed; embed()

    def _open_file(self):
        with open(self.filename, 'r') as file:
            data = self._format_to_json(file.read())
        return data

    def _format_to_json(self, file_data):
        output = json.loads(file_data)
        return output

    def _set_fabrics(self):
        self._set_project_materials_ids()
        for sub_project_material in self.data['projectMaterials']:
            self._main_or_lining_fabric(sub_project_material)

    def _enrich_project_materials(self):
        for project_material in self.data['projectMaterials']:
            for key, values in self._project_materials.items():
                if project_material['id'] in values['_raw']:
                    self._check_main_fabric(project_material, key)

    def _match_lining_to_curtain(self):
        for original_key, values in self._project_materials.items():
            for value in values['_raw']:
                item = self._project_materials.get(value)
                if item:
                    self._project_materials[original_key]['lining_fabric'] = \
                        item.get('lining_fabric')

    def _check_main_fabric(self, project_material, key):
        if str(project_material['categoryLinkInterfaceKey']) == 'Curtain.Fabric':
            self._project_materials[key]['main_fabric'] = \
                project_material['eanCode']
        if project_material['categoryLinkInterfaceKey'] == 'Lining.Fabric':
            self._project_materials[key]['lining_fabric'] = \
                project_material['eanCode']

    def _set_project_materials_ids(self):
        for project_material in self.data['projectMaterials']:
            if project_material.get('childIds'):
                self._project_materials[project_material['id']] = {
                    '_raw': project_material['childIds']
                }

    def _main_or_lining_fabric(self, sub_project_material):
        if str(sub_project_material['categoryLinkInterfaceKey']) == \
           'Curtain.Fabric':
            self._main_fabric = sub_project_material['eanCode']
        if sub_project_material['categoryLinkInterfaceKey'] == \
                'Lining.Fabric':
            self._lining_fabric = sub_project_material['eanCode']

    def _set_materials(self):
        for group in self.data['groups']:
            for room in group['rooms']:
                for polygon in room['polygons']:
                    for material in polygon['polygonProjectMaterials']:
                        curtain_key = material['projectMaterialId']
                        if curtain_key in self._project_materials.keys():
                            self._project_materials[curtain_key]['_polygon'] = polygon
                        self._analyse_project_polygons()

    def _analyse_project_polygons(self):
        for key, value in self._project_materials.items():
            self._project_materials[key]['properties'] = []
            self._check_polygon_project_materials(value, key)

    def _check_polygon_project_materials(self, polygon, key):
        if polygon.get('_polygon'):
            if polygon['_polygon'].get('polygonProjectMaterials'):
                if isinstance(
                    polygon['_polygon']['polygonProjectMaterials'],
                    list
                ):
                    for entry in polygon['_polygon']['polygonProjectMaterials']:
                        self._check_polygon_project_property(entry, key)

    def _check_polygon_project_property(self, polygon_project_material, key):
        if polygon_project_material.get('polygonProjectMaterialProperties'):
            if isinstance(
                    polygon_project_material['polygonProjectMaterialProperties'],
                    list
            ):
                for material in polygon_project_material['polygonProjectMaterialProperties']:
                    material = self._analyse_property(material, give=True)
                    self._project_materials[key]['properties'].append(material)


    def _analyse_property(self, item, give=False):
        if not item.get('isVisible'):
            pass
        curtain_property = dict(
            id=item['propertyId'],
            description=item['propertyDescription'],
            key=item['interfaceKey'],
            value=item['propertyValue'],
            visible=item['isVisible']
        )
        if give:
            return curtain_property

    def _identify_curtain_types(self):
        for key, value in self._project_materials.items():
            for item in value ['properties']:
                if item['key'] == 'Curtain.Fabric.Type':
                    self._project_materials[key]['curtain_type'] = \
                        self.CURTAIN_TYPES[item['value']]


    def _analyse_curtain_properties(self):
        for key, value in self._project_materials.items():
            curtain_type = value['curtain_type']
            if curtain_type == 'pleatcurtain':
                self._parse_pleatcurtain(value['properties'], key)
            if curtain_type == 'ringcurtain':
                self._parse_ringcurtain(value['properties'], key)
            if curtain_type == 'fronslintcurtain':
                self._parse_fronslintcurtain(value['properties'], key)

    def _parse_pleatcurtain(self, properties, key):
        for item in properties:
            if item['key'] in self.PLEATCURTAIN_TRANSLATOR.keys():
                self._project_materials[key][self.PLEATCURTAIN_TRANSLATOR[item['key']]] = item['value']
            if item['description'] in self.PLEATCURTAIN_DESCRIPTION_TRANSLATOR.keys():
                self._project_materials[key][self.PLEATCURTAIN_DESCRIPTION_TRANSLATOR[item['description']]] = item['value']

    def _parse_fronslintcurtain(self, properties):
        for item in properties:
            if item['key'] in self.FRONSLINTCURTAIN_TRANSLATOR.keys():
                self._curtain.update(
                    {item['key']: item['value']}
                )

    def _parse_ringcurtain(self, properties):
        for item in properties:
            if item['key'] in self.RINGCURTAIN_TRANSLATOR.keys():
                self._curtain.update(
                    {item['key']: item['value']}
                )
            if item['key'] in self.RINGCURTAIN_DESCRIPTION_TRANSLATOR.keys():
                self._curtain.update(
                    {item['key']: item['value']}
                )

def main():
    parser = CarpetRightJSONParser(filename='order_pleatcurtains.json')


    # # --- Legacy starts here ---
    # with open('002W.00010.json') as f:
    #     data = json.load(f)

    #     # Stof article number, voering?
    #     # Curtain types
    #     # # Width and height
    #         # links en rechts
    #     # Bruto / netto length
    #     # Plooi percentage
    #     # Plooi diepte, aantal


    #     # Sub types, pleat types
    #     # Kantelen

    #     # Knipmaten, Kamerhoog ongelijk stel, gelijk, stuk
    #     # Knipmaten, Banen ongelijk stel, gelijk, stuk

    #     # Voering
    #     # Knipmaten, Kamerhoog ongelijk stel, gelijk, stuk
    #     # Knipmaten, Banen ongelijk stel, gelijk, stuk

    #     # Prijs opbouw

    #     # CarpetRight FaceSide komt niet voor?
    #     # Key or id unique
    #     ORDER_ID = None

    #     print(f"carpet right order number: {data['prjNumber']}")
    #     print(f"carpet right order number: {data['prjDate']}")

    #     for project_material in data['projectMaterials'][0:2]:
    #         print(project_material['categoryId'])
    #         print(project_material['id'])

    #         if project_material['childIds']:
    #             for child_id in project_material['childIds']:
    #                 print(f'child_id: {child_id}')
    #                 for sub_project_material in data['projectMaterials']:
    #                     if child_id == sub_project_material['id']:
    #                         if str(sub_project_material['categoryLinkInterfaceKey']).endswith('.Fabric'):
    #                             print(f"main fabric with article nr { sub_project_material['eanCode']}")
    #                             MAIN_PROPERTIES = sub_project_material['id']

    #                         if sub_project_material['categoryInterfaceKey'] == 'Lining':
    #                             # Lining has 1 Child
    #                             find_id = sub_project_material['childIds'][0]
    #                             for sub_lining_project_material in data['projectMaterials']:
    #                                 if find_id == sub_lining_project_material['id']:
    #                                     print(f"lining fabric with article nr { sub_lining_project_material['eanCode']}")

    #         # for child_id in project_material['childIds']:
    #         #     print(f'child_id: {child_id}')
    #         for group in data['groups']:
    #             for room in group['rooms']:
    #                 for polygon in room['polygons']:
    #                     print("New orders")
    #                     print(polygon['lineNumber'])
    #                     # print(polygon['productCategory']['description'] )
    #                     for material in polygon['polygonProjectMaterials']:
    #                         # Status of order
    #                         # if material['polygonProjectMaterialStatuses']:
    #                         #     for status in material['polygonProjectMaterialStatuses']:
    #                         #         print("Status of orders")

    #                         # Properties of curtain
    #                         if material['polygonProjectMaterialProperties']:
    #                             if MAIN_PROPERTIES == material['projectMaterialId']:
    #                                 print(f"width: {polygon['width']}")
    #                                 print(f"height: {polygon['height']}")
    #                                 print(f"Property for order {MAIN_PROPERTIES}")
    #                                 for property in material['polygonProjectMaterialProperties']:
    #                                     analyse_property(
    #                                         id=property['propertyId'],
    #                                         description=property['propertyDescription'],
    #                                         key=property['interfaceKey'],
    #                                         value=property['propertyValue'],
    #                                         visible=property['isVisible']
    #                                     )



                            # print(material_status)


            # print(group)


if __name__ == "__main__":
    main()
